<?php

include "db.php";

// Verifica se o formulário foi enviado
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Coleta os dados do formulário
    $usuario = $_POST['usuario'];
    $senha = password_hash($_POST['senha'], PASSWORD_DEFAULT);

    // Prepara e executa a consulta SQL para inserir os dados
    $stmt = $pdo->prepare("INSERT INTO usuarios (usuario, senha) VALUES (?, ?)");
    $stmt->execute([$usuario, $senha]);

    header("Location:login.php");
    exit();
}
?>
