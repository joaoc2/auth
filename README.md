Auth

Overview

This PHP authentication application provides user registration with MD5 hash, login functionality, and redirection based on the success or failure of login attempts. Additionally, the application supports logout functionality and is connected to a database for persistent user data storage.

Features
User Registration: Secure user registration with password hashing using MD5.
Login: Authenticate users based on stored credentials in the database.
Redirection: Redirect users upon successful or unsuccessful login attempts.
Logout: Enable users to log out securely, terminating their current session.
Database Integration: Connect the application to a database for user data storage.
Getting Started
Prerequisites
Web server (e.g., Apache, Nginx)
PHP (>= 8.0)
MySQL database
Installation
Clone the repository:

bash
Copy code
git clone https://gitlab.com/joaoc2/auth.git
Configure your web server to serve the application.

Import the database schema from db.sql into your MySQL database.

Update the database connection details in db.php.

Access the application in your browser.

Usage
Visit the registration page (register.php) to create a new account.
Log in using your registered credentials on the login page (login.php).
Upon successful login, you will be redirected to the home page (index.php).
In case of a login error, you will be redirected to the error page (login_erro.php).
To log out, use the logout button on the home page (index.php).
Support
If you encounter any issues or have questions, please open an issue on the GitLab repository.

Contributing
Contributions are welcome! Please follow the guidelines in CONTRIBUTING.md.

Authors
Joao Costa
License
This project is licensed under the MIT License.

Project Status
This project is actively maintained. Feel free to contribute and improve it!
