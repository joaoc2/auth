<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="node_modules/bulma/css/bulma.min.css">
  <title>Tela de Registro</title>
  <style>
    /* Estilos personalizados, se necessário */
    body {
      margin: 20px;
    }

    /* Adicione um estilo para realçar em vermelho */
    .is-invalid {
      border-color: #ff3860 !important;
      box-shadow: 0 0 0 1px #ff3860 !important;
    }
  </style>
</head>
<body>

<section class="section">
  <div class="container">
    <div class="columns is-centered">
      <div class="column is-half">
        <h1 class="title is-1 has-text-centered">Registro de Usuarios</h1>
        
        <form method="POST" action="registro_valido.php" onsubmit="return validarFormulario()">
          <div class="field">
            <label class="label">Usuário</label>
            <div class="control">
              <input class="input" name="usuario" type="text" placeholder="Seu nome de usuário">
            </div>
          </div>

          <div class="field">
            <label class="label">Senha</label>
            <div class="control">
              <input class="input" id="senha" name="senha" type="password" placeholder="Sua senha" oninput="validarSenhas()">
            </div>
          </div>

          <div class="field">
            <label class="label">Confirme a Senha</label>
            <div class="control">
              <input class="input" id="confirmaSenha" name="confirmaSenha" type="password" placeholder="Confirme sua senha" oninput="validarSenhas()">
            </div>
          </div>

          <div class="field is-grouped">
            <div class="control">
              <button class="button is-primary" id="botaoRegistrar" disabled>Registrar</button>
            </div>
          </div>
        </form>

      </div>
    </div>
  </div>
</section>

<script src="js/registro_validar.js"></script>

</body>
</html>
