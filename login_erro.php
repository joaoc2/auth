<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
  <title>Login Erro</title>
  <style>
    body {
      margin: 20px;
    }

    .login-container {
      max-width: 400px;
      margin: auto;
      margin-top: 50px;
    }
  </style>
</head>
<body>

<section class="section">
  <div class="container login-container">
    <div class="box">
      <h1 class="title is-3 has-text-centered">Erro de Login</h1>
      <p class="has-text-danger has-text-centered">Usuário ou senha incorretos. Tente novamente.</p>
      <div class="field is-grouped">
        <div class="control">
          <a href="login.php" class="button is-primary">Voltar para a Página Inicial</a>
        </div>
      </div>
    </div>
  </div>
</section>

</body>
</html>
