<?php
session_start();
// Verifica se o usuário está autenticado
if (!isset($_SESSION['usuario'])) {
    // Se não estiver autenticado, redireciona para a página de login
    header("Location: login.php");
    exit();
}

// Obtém o nome do usuário da sessão
$nomeUsuario = $_SESSION['usuario'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
  <title>Página Inicial</title>
  <style>
    body {
      margin: 20px;
    }

    .index-container {
      max-width: 400px;
      margin: auto;
      margin-top: 50px;
    }
  </style>
</head>
<body>

<section class="section">
  <div class="container index-container">
    <div class="box">
      <h1 class="title is-3 has-text-centered">Olá, <?php echo $nomeUsuario;?></h1>
      <p class="has-text-centered">Bem-vindo à sua página inicial.</p>
      <div class="field is-grouped">
        <div class="control">
          <a href="desconectado.php" class="button is-success">fazer logoff</a>
        </div>
      </div>
    </div>
  </div>
</section>

</body>
</html>
