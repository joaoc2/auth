function validarUsuario() {
    const usuario = document.querySelector('input[name="usuario"]');
    const pass = document.querySelector('input[name="senha"]');
    const botaoRegistrar = document.getElementById('botaoRegistrar');

    if (usuario.value.length > 16) {
        usuario.classList.add('is-invalid');
        alert('Usuário muito grande. O tamanho máximo é 16 caracteres.');
    } else {
        usuario.classList.remove('is-invalid');
    }

    

}

function validarSenhas() {
    const senha = document.getElementById('senha');
    const confirmaSenha = document.getElementById('confirmaSenha');
    const botaoRegistrar = document.getElementById('botaoRegistrar');

    if (senha.value !== confirmaSenha.value || senha.value === '' || confirmaSenha.value === '') {
        senha.classList.add('is-invalid');
        confirmaSenha.classList.add('is-invalid');
        botaoRegistrar.disabled = true;
    } else {
        senha.classList.remove('is-invalid');
        confirmaSenha.classList.remove('is-invalid');
        botaoRegistrar.disabled = false;
    }

    // Adicionando a validação do tamanho do usuário
    validarUsuario();
}

function validarFormulario() {
    const senha = document.getElementById('senha');
    const confirmaSenha = document.getElementById('confirmaSenha');

    if (senha.value !== confirmaSenha.value) {
        alert('As senhas não coincidem!');
        return false;
    }

    return true;
}
