<?php


include "db.php";

// Verifica se o formulário foi enviado
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Coleta os dados do formulário
    $usuario = $_POST['usuario'];
    $senha = $_POST['senha'];

    // Prepara e executa a consulta SQL para verificar as credenciais
    $stmt = $pdo->prepare("SELECT * FROM usuarios WHERE usuario = ?");
    $stmt->execute([$usuario]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    
    
    // Verifica se o usuário existe
    if ($user) {
        // Mensagens de depuração
        echo "Hash do banco de dados: " . $user['senha'] . "<br>";
        echo "Senha fornecida (antes do hash): $senha<br>";

        // Verifica se a senha está correta
        if (password_verify($senha, $user['senha'])) {
            session_start();
            $_SESSION['usuario'] = $user['usuario'];
            // Login bem-sucedido, redireciona para a página desejada
            header("Location: index.php");
            exit();
        } else {
            // Mensagem de erro
            header("Location:login_erro.php");
            exit();

            
        }
    } else {
        // Mensagem de erro
        echo "Usuário não encontrado<br>";

        
    }
}
?>




<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="node_modules/bulma/css/bulma.min.css">
  <title>Tela de Login</title>
  <style>
    /* Estilos personalizados, se necessário */
    body {
      margin: 20px;
    }
  </style>
</head>
<body>

<section class="section">
  <div class="container login-container">
    <div class="box">
      <h1 class="title is-3 has-text-centered">Login</h1>

      <!-- Adicionado method="post" e action="seu_script_php.php" ao formulário -->
      <form method="post" action="login.php">
        <div class="field">
          <label class="label">Nome de usuário</label>
          <div class="control">
            <input class="input" name="usuario" type="text" placeholder="Seu nome de usuário">
          </div>
        </div>

        <div class="field">
          <label class="label">Senha</label>
          <div class="control">
            <input class="input" type="password" name="senha" placeholder="Sua senha">
          </div>
        </div>

        <div class="field is-grouped">
          <div class="control">
            <button class="button is-primary">Entrar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>

</body>
</html>

