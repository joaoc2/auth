<?php
// Configurações do banco de dados
$servername = "127.0.0.1";
$username = "root";
$password = "";
$dbname = "auth";

// Conexão com MySQLi
$mysqli = new mysqli($servername, $username, $password, $dbname);

// Verifica a conexão com MySQLi
if ($mysqli->connect_error) {
    die("Erro na conexão MySQLi: " . $mysqli->connect_error);
}

// Conexão com PDO
try {
    $pdo = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Erro na conexão PDO: " . $e->getMessage());
}
?>
